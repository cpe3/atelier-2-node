// création serveur de fichiers statiques avec express -> fichiers contenus dans www
// const { json } = require('express');
const express = require('express')
const app = express()
app.use(express.static('www'));

// On écoute sur le port spécifié
const port = 3001;
const server = require('http').createServer(app);
server.listen(port, () => {
  console.log(`listening on : http://127.0.0.1:${port}`);
});

// création de la connection par socket
const io = require('socket.io');
const ioServer = io(server);

// Variables
const userList = []; // Liste des utilisateurs
const socketList = []; // Liste des sockets
const messageList = [];

ioServer.on('connection', function (socket) {
  socketList.push(socket);

  // User connection
  socket.on('userIn',function(data) {
    let userAlreadyInList = false;
    for (var i=0; i<userList.length; i++) { //iterate through each object in an array
      if (JSON.stringify(userList[i]) == JSON.stringify(data) ) {
          userAlreadyInList = true;
       }
    }
    if (!userAlreadyInList) {
      userList.push(data);
      socket.login = data.login;
      console.log("SOCKET USERNAME : " + socket.login);
      ioServer.emit('userConnected',userList);
    }
  })

  // User Wants to FIGHT
  socket.on('sendFightRequest',(data) =>{
    user_sender = socket.login;
    user_target = data.login; 
    if (!(user_sender == user_target))//verification qu'il ne se defie pas lui meme  
        for (const element of socketList) {//Parcourt l'ensemble des sockets actives pour trouver la cible
            if (element.login == user_target){
                element.emit('sendFightAlert',user_sender);
            }
        }
  });

  // New message connection
  socket.on('newMessage',function(message) {
    messageList.push(socket.login + ' : ' + message);
    ioServer.emit('getMessageList',messageList);
  })

});